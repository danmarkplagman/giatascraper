<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHotelImagesColumnsIntoNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_images', function (Blueprint $table) {
            $table->integer('width')->nullable()->change();
            $table->integer('height')->nullable()->change();
            $table->string('url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_images', function (Blueprint $table) {
            $table->integer('width')->change();
            $table->integer('height')->change();
            $table->string('url')->change();
        });
    }
}
