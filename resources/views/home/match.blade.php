@extends('layouts.master')
@section('heading', 'Hotel: '.$hotel->name)

@section('content')
<div>
    <h4>Matches:</h4>

    @if ($items->count() > 0)
    @foreach ($items as $item)
    @if ($item->images->count() > 0)
    <div class="card">
        <div class="card-header">
            {{ $item->name }}
            <div class="float-right">
                @if ($items->count() > 1)
                <button class="btn btn-primary btn-sm">Select as Match</button>
                @endif
            </div>
        </div>
        <div class="card-body">
            @foreach ($item->images as $image)
            <img src="{{ authImage($image->url) }}" width="150">
            @endforeach
        </div>
        <div class="card-footer">
            <div clas="float-left">Address: {{ $item->street }}, {{ $item->city }}, {{ $item->country }}</div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    @else
    <div>No match found or is no images found on match</div>
    @endif
    @endforeach
    @else
    <div>No match found</div>
    @endif

</div>
@endsection