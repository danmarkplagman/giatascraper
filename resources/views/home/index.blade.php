@extends('layouts.master')
@section('heading', 'JesRes 4 and 5 Star Hotels')

@section('content')
<div>
<label>Select hotel to begin matching</label>
<ul>
    @if ($hotels->count() > 0)
    @foreach ($hotels as $hotel)
    <li>
        <a href="{{ route('home.match', $hotel->id) }}">{{ $hotel->name }}</a>
    </li>
    @endforeach
    @endif
</ul>
</div>

<div>{{ $hotels->links() }}</div>
@endsection