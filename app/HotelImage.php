<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelImage extends Model
{
    protected $fillable = [
        'hotel_id',
        'width',
        'height',
        'url'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
}
