<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
        'jesres_hotel_id',
        'hotel_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    
}
