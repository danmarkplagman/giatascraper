<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\JesresHotel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $hotels = JesresHotel::paginate(10);

        return view('home.index')->with(compact('hotels'));
    }

    public function match($id)
    {
        $hotel = JesresHotel::find($id);
        $items = Hotel::where([
            ['name', 'LIKE', "%{$hotel->name}%"]
        ])
        ->get();
        
        return view('home.match')->with(compact('hotel','items'));
    }
}
