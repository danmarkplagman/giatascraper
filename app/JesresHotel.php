<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JesresHotel extends Model
{
    protected $fillable = [
        'id',
        'name',
        'address_1',
        'address_2',
        'city',
        'state',
        'country'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];  
}
