<?php

namespace App\Console\Commands;

use App\Hotel;
use Illuminate\Console\Command;

class MatchDatabaseHotels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:db_hotel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $jesres_hotels = file_get_contents(storage_path().'/json/hotel_lists.json');
        $jesres_hotels = json_decode($jesres_hotels);
        $hotels = [];

        echo 'Start matching...'.PHP_EOL;

        foreach ($jesres_hotels as $hotel) {

            $search = Hotel::where('name', 'LIKE', "%{$hotel->name}%")->get();            

            if ($search->count() > 0) {

                foreach ($search as $data) {

                    $hotels[] = [
                        'name'      => $hotel->name,
                        'address'   => [
                            'address_1' => $hotel->address_1,
                            'address_2' => $hotel->address_2,
                            'city'      => $hotel->city,
                            'state'     => $hotel->state,
                            'country'   => $hotel->country
                        ],
                        'matches'   => [
                            'id'        => $data->id,
                            'giata_id'  => $data->giata_id,
                            'name'      => $data->name,
                            'address'   => [
                                'street'    => $data->street,
                                'city'      => $data->city,
                                'country'   => $data->country
                            ]
                        ]
                    ];
                }
            }
        }

        file_put_contents(storage_path() . '/json/hotel_matches.json', $hotels);

        echo 'Finished.';
    }
}
