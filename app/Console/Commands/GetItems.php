<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use DomDocument;

class GetItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Giata Items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $doc = new DOMDocument();

        $res = $client->request('GET', 'http://ghgml.giatamedia.com/webservice/rest/1.0/items/', [
            'auth' => ['hello|jesres.com', 'Pkefw5HK'],
            'http_errors' => false
        ]);

        $data = $res->getBody();
        
        $doc->loadXML($data);

        $resourceRefs = $doc->getElementsByTagName('item');

        $hotels = [];
        $count = 0;

        $this->output->write('fetching...', false);

        foreach($resourceRefs as $ref) {

            $count += 1;
            // $this->_getXML($ref->getAttributeNS('http://www.w3.org/1999/xlink', 'href'), $count);
        }

        $this->output->write('finished.', false);
        $this->output->write($count, false);
    }

    private function _getXML($link, $count)
    {
        $client = new Client();
        $doc    = new DOMDocument();

        $res = $client->request('GET', $link, [
            'auth' => ['hello|jesres.com', 'Pkefw5HK'],
            'http_errors' => false
        ]);
        
        $data = $res->getBody();
        $doc->loadXML($data);
        
        // file_put_contents(storage_path() . '/xml/hotels/'.$doc->getElementsByTagName('item')[0]->getAttribute('giataId').'.xml', $data);
        file_put_contents(storage_path() . '/xml/hotels/'.$count.'.xml', $data);
    }
}
