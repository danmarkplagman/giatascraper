<?php

namespace App\Console\Commands;

use App\Hotel;
use App\HotelImage;
use Illuminate\Console\Command;

class SaveDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $items = file_get_contents(storage_path().'/json/giata_items.json');
        $items = json_decode($items);

        echo 'Saving to DB...';

        foreach ($items as $item) {

            $hotel = Hotel::create([
                'giata_id'  => $item->giata_id,
                'name'      => $item->name,
                'street'    => $item->address->street,
                'city'      => $item->address->city,
                'country'   => $item->address->country
            ]);

            foreach ($item->images as $image) {

                HotelImage::create([
                    'hotel_id'  => $hotel->id,
                    'width'     => $image->width,
                    'height'    => $image->height,
                    'url'       => $image->url
                ]);
            }
        }

        echo 'Finished.';
    }
}
