<?php

namespace App\Console\Commands;

use App\Hotel;
use App\Match;
use Exception;
use DOMDocument;
use App\JesresHotel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class MatchHotels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:hotel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match Hotel Name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        echo 'starting...'.PHP_EOL;

        // $items = [];
    
        $jesresHotels = JesresHotel::get();

        foreach ($jesresHotels as $jesresHotel) {

            // $images = [];
            // $matches = [];

            $hotels = Hotel::where([
                ['name', 'LIKE', "%{$jesresHotel->name}%"]
            ])
            ->get();

            if ($hotels->count() > 0) {

                foreach ($hotels as $hotel) {

                    if ($hotel->images->count() > 0) {

                        Match::create([
                            'jesres_hotel_id'   => $jesresHotel->id,
                            'hotel_id'          => $hotel->id
                        ]);

                        // foreach ($hotel->images as $image) {
                            // $images[] = [
                            //     'url'       => $image->url,
                            //     'width'     => $image->width,
                            //     'height'    => $image->height
                            // ];
                        // }
                    }

                    // $matches[] = [
                    //     'giata_id'  => $hotel->giata_id,
                    //     'name'      => $hotel->name,
                    //     'street'    => $hotel->street,
                    //     'city'      => $hotel->city,
                    //     'country'   => $hotel->country,
                    //     'images'    => $images
                    // ];

                }

            }

            // $items[] = [
            //     'id'        => $jesresHotel->id,
            //     'name'      => $jesresHotel->name,
            //     'matches'   => $matches
            // ];
        }
        
        // Done!
        echo 'finished.'.PHP_EOL;

    }
}
