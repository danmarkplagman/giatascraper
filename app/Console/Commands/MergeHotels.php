<?php

namespace App\Console\Commands;

use DOMDocument;
use Illuminate\Console\Command;

class MergeHotels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merge:hotel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        
        $hotels = [];

        echo 'Starting...';

        for($i = 1; $i <= 265027; $i++) {
            echo $i.PHP_EOL;
            $xml = file_get_contents(storage_path() . '/xml/hotels/'.$i.'.xml');

            $dom = new DOMDocument();

            $dom->loadXML($xml);
            
            $item = $dom->getElementsByTagName('item');
            $name = $dom->getElementsByTagName('name');
            $sizes = $dom->getElementsByTagName('size');
            $street = $dom->getElementsByTagName('street');
            $city = $dom->getElementsByTagName('city');
            $country = $dom->getElementsByTagName('country');

            if(!empty($name[0]->nodeValue)) {

                $images = [];
                foreach($sizes as $size) {
                    $images[] = [
                        'width' => $size->getAttribute('width'),
                        'height' => $size->getAttribute('height'),
                        'url' => $size->getAttributeNS('http://www.w3.org/1999/xlink', 'href')
                    ];
                }
        
                $hotels[] = [
                    'giata_id' => $item[0]->getAttribute('giataId'),
                    'name' => $name[0]->nodeValue,
                    'address' => [
                        'street' => $street[0]->nodeValue,
                        'city' => $city[0]->nodeValue,
                        'country' => $this->_mapCountry($country[0]->nodeValue)
                    ],
                    'images' => $images
                ];
    
            }
        }
        
        file_put_contents(storage_path() . '/json/giata_items.json', json_encode($hotels, JSON_PRETTY_PRINT));

        echo PHP_EOL.'Done.';
    }

    private function _mapCountry($country_code) {

        $json = file_get_contents(storage_path() . '/json/countries.json');
        $countries = json_decode($json);

        foreach($countries as $country) {

            if($country->code == $country_code) {
                return $country->name;
            }
        }
    }
}
