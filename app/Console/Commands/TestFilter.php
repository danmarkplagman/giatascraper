<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestFilter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:filter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $giata_hotels = file_get_contents(storage_path().'/json/giata_items.json');
        $giata_hotels = json_decode($giata_hotels);
        $data = [];

        foreach ($giata_hotels as $giata) {
            if (fnmatch('*quinta do lago*', $giata->name)) {
                $data[] = $giata->name;
            }
        }

        file_put_contents(storage_path() . '/json/test.json', json_encode($data, JSON_PRETTY_PRINT));        
    }
}
