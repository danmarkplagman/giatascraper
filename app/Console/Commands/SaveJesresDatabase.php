<?php

namespace App\Console\Commands;

use App\JesresHotel;
use Illuminate\Console\Command;

class SaveJesresDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:jesres';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $items = file_get_contents(storage_path().'/json/hotel_lists.json');
        $items = json_decode($items);

        echo 'Saving to DB...';

        foreach ($items as $item) {

            $hotel = JesresHotel::create([
                'name'      => $item->name,
                'address_1' => $item->address_1,
                'address_2' => $item->address_2,
                'city'      => $item->city,
                'state'     => $item->state,
                'country'   => $item->country
            ]);
        }

        echo 'Finished.';
    }
}
